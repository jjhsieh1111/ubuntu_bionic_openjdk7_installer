# ubuntu_bionic_openjdk7_installer
Install Zulu OpenJDK7 for Ubuntu Bionic (18.04)

There is no OpenJDK7 .deb packages for Ubuntu 18.04
so I need to manually download one from Zulu (Thanks!)

https://www.azul.com/downloads/zulu/zulu-linux/

But the update-alternatives and update-java-alternatives
do not automatically work so I need to do extra steps in
install.sh.

The install.sh does the following:
1. Download OpenJDK7 tar ball
2. Extract the tar ball to /usr/lib/jvm
3. Setup update-alternatives for every binaries
4. Copy .java-1.7.0-openjdk-amd64.jinfo for update-java-alternatives to work

After successfully installed, run the following
to switch to OpenJDK7:

$ sudo update-java-alternatives --set java-1.7.0-openjdk-amd64
