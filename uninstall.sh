#!/bin/sh

JAVA_HOME=/usr/lib/jvm/zulu7.23.0.1-jdk7.0.181-linux_x64
sudo update-alternatives --remove rmid ${JAVA_HOME%*/}/jre/bin/rmid
sudo update-alternatives --remove java ${JAVA_HOME%*/}/jre/bin/java
sudo update-alternatives --remove keytool ${JAVA_HOME%*/}/jre/bin/keytool
sudo update-alternatives --remove pack200 ${JAVA_HOME%*/}/jre/bin/pack200
sudo update-alternatives --remove rmiregistry ${JAVA_HOME%*/}/jre/bin/rmiregistry
sudo update-alternatives --remove unpack200 ${JAVA_HOME%*/}/jre/bin/unpack200
sudo update-alternatives --remove orbd ${JAVA_HOME%*/}/jre/bin/orbd
sudo update-alternatives --remove servertool ${JAVA_HOME%*/}/jre/bin/servertool
sudo update-alternatives --remove nameserv ${JAVA_HOME%*/}/jre/bin/tnameserv
sudo update-alternatives --remove policytool ${JAVA_HOME%*/}/jre/bin/policytool
sudo update-alternatives --remove idlj ${JAVA_HOME%*/}/bin/idlj
sudo update-alternatives --remove wsimport ${JAVA_HOME%*/}/bin/wsimport
sudo update-alternatives --remove jinfo ${JAVA_HOME%*/}/bin/jinfo
sudo update-alternatives --remove jsadebugd ${JAVA_HOME%*/}/bin/jsadebugd
sudo update-alternatives --remove native2ascii ${JAVA_HOME%*/}/bin/native2ascii
sudo update-alternatives --remove jstat ${JAVA_HOME%*/}/bin/jstat
sudo update-alternatives --remove javac ${JAVA_HOME%*/}/bin/javac
sudo update-alternatives --remove javah ${JAVA_HOME%*/}/bin/javah
sudo update-alternatives --remove jstack ${JAVA_HOME%*/}/bin/jstack
sudo update-alternatives --remove jrunscript ${JAVA_HOME%*/}/bin/jrunscript
sudo update-alternatives --remove javadoc ${JAVA_HOME%*/}/bin/javadoc
sudo update-alternatives --remove javap ${JAVA_HOME%*/}/bin/javap
sudo update-alternatives --remove jar ${JAVA_HOME%*/}/bin/jar
sudo update-alternatives --remove xjc ${JAVA_HOME%*/}/bin/xjc
sudo update-alternatives --remove schemagen ${JAVA_HOME%*/}/bin/schemagen
sudo update-alternatives --remove jps ${JAVA_HOME%*/}/bin/jps
sudo update-alternatives --remove extcheck ${JAVA_HOME%*/}/bin/extcheck
sudo update-alternatives --remove rmic ${JAVA_HOME%*/}/bin/rmic
sudo update-alternatives --remove jstatd ${JAVA_HOME%*/}/bin/jstatd
sudo update-alternatives --remove jhat ${JAVA_HOME%*/}/bin/jhat
sudo update-alternatives --remove jdb ${JAVA_HOME%*/}/bin/jdb
sudo update-alternatives --remove serialver ${JAVA_HOME%*/}/bin/serialver
sudo update-alternatives --remove wsgen ${JAVA_HOME%*/}/bin/wsgen
sudo update-alternatives --remove jcmd ${JAVA_HOME%*/}/bin/jcmd
sudo update-alternatives --remove jarsigner ${JAVA_HOME%*/}/bin/jarsigner
sudo update-alternatives --remove jmap ${JAVA_HOME%*/}/bin/jmap
sudo update-alternatives --remove appletviewer ${JAVA_HOME%*/}/bin/appletviewer
sudo update-alternatives --remove jconsole ${JAVA_HOME%*/}/bin/jconsole

sudo rm -rf ${JAVA_HOME} /usr/lib/jvm/java-1.7.0-openjdk-amd64 /usr/lib/jvm/.java-1.7.0-openjdk-amd64.jinfo
