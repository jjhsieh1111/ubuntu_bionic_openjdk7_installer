#!/bin/sh
#https://www.azul.com/downloads/zulu/zulu-linux/
file="./zulu7.23.0.1-jdk7.0.181-linux_x64.tar.gz"
if [ ! -f $file ]
then
echo "Downloading $file"
wget https://cdn.azul.com/zulu/bin/zulu7.23.0.1-jdk7.0.181-linux_x64.tar.gz
fi

if [ -f $file ]
then
echo "Extrating $file to /usr/lib/jvm"
sudo tar xvf $file -C /usr/lib/jvm/
else
echo "$file does not exist!"
exit 1
fi

JAVA_HOME=/usr/lib/jvm/zulu7.23.0.1-jdk7.0.181-linux_x64
sudo update-alternatives --install /usr/bin/rmid rmid ${JAVA_HOME%*/}/jre/bin/rmid 0
sudo update-alternatives --install /usr/bin/java java ${JAVA_HOME%*/}/jre/bin/java 0
sudo update-alternatives --install /usr/bin/keytool keytool ${JAVA_HOME%*/}/jre/bin/keytool 0
sudo update-alternatives --install /usr/bin/pack200 pack200 ${JAVA_HOME%*/}/jre/bin/pack200 0
sudo update-alternatives --install /usr/bin/rmiregistry rmiregistry ${JAVA_HOME%*/}/jre/bin/rmiregistry 0
sudo update-alternatives --install /usr/bin/unpack200 unpack200 ${JAVA_HOME%*/}/jre/bin/unpack200 0
sudo update-alternatives --install /usr/bin/orbd orbd ${JAVA_HOME%*/}/jre/bin/orbd 0
sudo update-alternatives --install /usr/bin/servertool servertool ${JAVA_HOME%*/}/jre/bin/servertool 0
sudo update-alternatives --install /usr/bin/tnameserv tnameserv ${JAVA_HOME%*/}/jre/bin/tnameserv 0
sudo update-alternatives --install /usr/bin/policytool policytool ${JAVA_HOME%*/}/jre/bin/policytool 0
sudo update-alternatives --install /usr/bin/idlj idlj ${JAVA_HOME%*/}/bin/idlj 0
sudo update-alternatives --install /usr/bin/wsimport wsimport ${JAVA_HOME%*/}/bin/wsimport 0
sudo update-alternatives --install /usr/bin/jinfo jinfo ${JAVA_HOME%*/}/bin/jinfo 0
sudo update-alternatives --install /usr/bin/jsadebugd jsadebugd ${JAVA_HOME%*/}/bin/jsadebugd 0
sudo update-alternatives --install /usr/bin/native2ascii native2ascii ${JAVA_HOME%*/}/bin/native2ascii 0
sudo update-alternatives --install /usr/bin/jstat jstat ${JAVA_HOME%*/}/bin/jstat 0
sudo update-alternatives --install /usr/bin/javac javac ${JAVA_HOME%*/}/bin/javac 0
sudo update-alternatives --install /usr/bin/javah javah ${JAVA_HOME%*/}/bin/javah 0
sudo update-alternatives --install /usr/bin/jstack jstack ${JAVA_HOME%*/}/bin/jstack 0
sudo update-alternatives --install /usr/bin/jrunscript jrunscript ${JAVA_HOME%*/}/bin/jrunscript 0
sudo update-alternatives --install /usr/bin/javadoc javadoc ${JAVA_HOME%*/}/bin/javadoc 0
sudo update-alternatives --install /usr/bin/javap javap ${JAVA_HOME%*/}/bin/javap 0
sudo update-alternatives --install /usr/bin/jar jar ${JAVA_HOME%*/}/bin/jar 0
sudo update-alternatives --install /usr/bin/xjc xjc ${JAVA_HOME%*/}/bin/xjc 0
sudo update-alternatives --install /usr/bin/schemagen schemagen ${JAVA_HOME%*/}/bin/schemagen 0
sudo update-alternatives --install /usr/bin/jps jps ${JAVA_HOME%*/}/bin/jps 0
sudo update-alternatives --install /usr/bin/extcheck extcheck ${JAVA_HOME%*/}/bin/extcheck 0
sudo update-alternatives --install /usr/bin/rmic rmic ${JAVA_HOME%*/}/bin/rmic 0
sudo update-alternatives --install /usr/bin/jstatd jstatd ${JAVA_HOME%*/}/bin/jstatd 0
sudo update-alternatives --install /usr/bin/jhat jhat ${JAVA_HOME%*/}/bin/jhat 0
sudo update-alternatives --install /usr/bin/jdb jdb ${JAVA_HOME%*/}/bin/jdb 0
sudo update-alternatives --install /usr/bin/serialver serialver ${JAVA_HOME%*/}/bin/serialver 0
sudo update-alternatives --install /usr/bin/wsgen wsgen ${JAVA_HOME%*/}/bin/wsgen 0
sudo update-alternatives --install /usr/bin/jcmd jcmd ${JAVA_HOME%*/}/bin/jcmd 0
sudo update-alternatives --install /usr/bin/jarsigner jarsigner ${JAVA_HOME%*/}/bin/jarsigner 0
sudo update-alternatives --install /usr/bin/jmap jmap ${JAVA_HOME%*/}/bin/jmap 0
sudo update-alternatives --install /usr/bin/appletviewer appletviewer ${JAVA_HOME%*/}/bin/appletviewer 0
sudo update-alternatives --install /usr/bin/jconsole jconsole ${JAVA_HOME%*/}/bin/jconsole 0

sudo ln -f -s ${JAVA_HOME} /usr/lib/jvm/java-1.7.0-openjdk-amd64
sudo cp .java-1.7.0-openjdk-amd64.jinfo /usr/lib/jvm/.java-1.7.0-openjdk-amd64.jinfo
